var bank_accountModel = require('./bank_accountModel.js');
var withdrawModel = require('../withdraw/withdrawModel');
var moment = require('moment');
var config = require('../config');
var paystack = require('paystack-api')(config.PAYSTACK);
var userModel = require('./../user/userModel');
const { check, validationResult } = require('express-validator');

/**
 * bank_accountController.js
 *
 * @description :: Server-side logic for managing bank_accounts.
 */
module.exports = {

    /**
     * bank_accountController.list()
     */
    list: function (req, res) { 
        bank_accountModel.find({user_id: req.user}, function (err, bank_accounts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting bank_account.',
                    error: err
                });
            }
            return res.json(bank_accounts);
        });
    },

    /**
     * bank_accountController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        bank_accountModel.findOne({_id: id}, function (err, bank_account) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting bank_account.',
                    error: err
                });
            }
            if (!bank_account) {
                return res.status(404).json({
                    message: 'No such bank_account'
                });
            }
            return res.json(bank_account);
        });
    },

    /**
     * bank_accountController.create()
     */
    create: async function (req, res) {
        const ValErrors = validationResult(req);

        if (!ValErrors.isEmpty()) {
            return res.status(500).json({
                val_message: ValErrors.array()[0]['msg'],
                error: ValErrors.array()[0]['msg']
            });
        }else{
            paystack.transfer_recipient
        .create({
            type: 'nuban',
            name: req.body.name,
            description: req.body.name+' Transfer Reciepent',
            bank_code: req.body.bank,
            currency:'NGN',
            account_number: req.body.account_number
        })
        .then((body)=>{

           // return res.status(200).json(body.data.recipient_code);

            if(req.body._id){
                bank_accountModel.findOne({_id: req.body._id}, function (err, bank_account) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error when getting bank_account',
                            error: err
                        });
                    } 
                    if (!bank_account) {
                        return res.status(404).json({
                            message: 'No such bank_account'
                        });
                    }
        
                    bank_account.user_id = req.user;
                    bank_account.bank = req.body.bank ? req.body.bank : bank_account.bank;
                    bank_account.name = req.body.name ? req.body.name : bank_account.name;
                    bank_account.sort = req.body.sort ? req.body.sort : bank_account.sort;
                    bank_account.account_ref = body.data.recipient_code;
                    //bank_account.bvn = req.body.bvn ? req.body.bvn : bank_account.bvn;
                    bank_account.account_number = req.body.account_number ? req.body.account_number : bank_account.account_number;
                    
                    bank_account.save(function (err, bank_account) {
                        if (err) {
                            return res.status(500).json({
                                message: 'Error when updating bank_account.',
                                error: err
                            });
                        }
                        var withdraw = new withdrawModel({
                            user_id : req.user,
                            account_id : bank_account._id,
                            amount : req.body.amount,
                            status : false,
                            date : moment().fromNow()
                
                        });
                        withdraw.save();
                        
                        userModel.updateOne({_id: req.user}, { $inc: { balance: -parseInt(req.body.amount) }}).exec();

                        return res.json(bank_account);
                    });
                });
    
            }else{
                var bank_account = new bank_accountModel({
                    name : req.body.name,
                    sort : req.body.sort,
                    bank : req.body.bank,
                   // bvn : req.body.bvn,
                    account_ref: body.data.recipient_code,
                    account_number : req.body.account_number,
                    user_id : req.user
                });
    
                bank_account.save(function (err, bank_account) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error when creating bank_account',
                            error: err
                        });
                    }
                    var withdraw = new withdrawModel({
                        user_id : req.user,
                        account_id : bank_account._id,
                        amount : req.body.amount,
                        status : false,
                        date : moment().fromNow()
                    });
                    withdraw.save();
                    userModel.updateOne({_id: req.user}, { $inc: { balance: -parseInt(req.body.amount) }}).exec();
                    return res.status(200).json(bank_account);
                });
    
            }

        })
        .catch((error)=>{
            if(error){
                    return res.status(500).json({
                        message: error,
                        error: error
                    });
                }
        });
        }

    },

    verifyAccount: function(req, callback){
        paystack.transfer_recipient
        .create({
            name: req.body.name,
            description: req.body.name+' Transfer Reciepent',
            bank_code: req.body.bank,
            currency:'NGN',
            account_number: req.body.account_number
        }).then((error, body)=>{
            callback(error, body);
        });

    },

    /**
     * bank_accountController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        bank_accountModel.findOne({_id: id}, function (err, bank_account) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting bank_account',
                    error: err
                });
            }
            if (!bank_account) {
                return res.status(404).json({
                    message: 'No such bank_account'
                });
            }

            bank_account.name = req.body.name ? req.body.name : bank_account.name;
			bank_account.sort = req.body.sort ? req.body.sort : bank_account.sort;
			bank_account.bvn = req.body.bvn ? req.body.bvn : bank_account.bvn;
			bank_account.account_number = req.body.account_number ? req.body.account_number : bank_account.account_number;
			bank_account.user_id = req.body.user_id ? req.body.user_id : bank_account.user_id;
			
            bank_account.save(function (err, bank_account) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating bank_account.',
                        error: err
                    });
                }

                return res.json(bank_account);
            });
        });
    },

    /**
     * bank_accountController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        bank_accountModel.findByIdAndRemove(id, function (err, bank_account) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the bank_account.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
