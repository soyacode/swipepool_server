var paymentModel = require('./paymentModel.js');
var moment = require('moment');
var userModel = require('../user/userModel');

/**
 * paymentController.js
 *
 * @description :: Server-side logic for managing payments.
 */
module.exports = {

    /**
     * paymentController.list()
     */
    list: function (req, res) {
        paymentModel.find(function (err, payments) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting payment.',
                    error: err
                });
            }
            return res.json(payments);
        });
    },

    /**
     * paymentController.list()
     */
    confirm: function (req, res) {
        var payment = new paymentModel({
			amount : req.body.amount,
			status : true,
			user_id : req.user,
			reference : req.body.reference,
			moves : parseInt(req.body.moves),
			gateway : 'PAYSTACK',
			paid_on : moment().date()
        });

        payment.save(function (err, payment) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating payment',
                    error: err
                });
            }
            userModel.findOneAndUpdate({ _id: payment.user_id }, { $inc: { moves: payment.moves } }, {new: true }).exec();
            return res.status(201).json(payment);
        });
    },
 
    /**
     * paymentController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        paymentModel.findOne({_id: id}, function (err, payment) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting payment.',
                    error: err
                });
            }
            if (!payment) {
                return res.status(404).json({
                    message: 'No such payment'
                });
            }
            return res.json(payment);
        });
    },

    /**
     * paymentController.create()
     */
    create: function (req, res) {
        var payment = new paymentModel({
			amount : req.body.amount,
			status : req.body.status,
			user_id : req.body.user_id,
			gateway : req.body.gateway,
			paid_on : req.body.paid_on

        });

        payment.save(function (err, payment) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating payment',
                    error: err
                });
            }
            return res.status(201).json(payment);
        });
    },

    /**
     * paymentController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        paymentModel.findOne({_id: id}, function (err, payment) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting payment',
                    error: err
                });
            }
            if (!payment) {
                return res.status(404).json({
                    message: 'No such payment'
                });
            }

            payment.amount = req.body.amount ? req.body.amount : payment.amount;
			payment.status = req.body.status ? req.body.status : payment.status;
			payment.user_id = req.body.user_id ? req.body.user_id : payment.user_id;
			payment.gateway = req.body.gateway ? req.body.gateway : payment.gateway;
			payment.paid_on = req.body.paid_on ? req.body.paid_on : payment.paid_on;
			
            payment.save(function (err, payment) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating payment.',
                        error: err
                    });
                }

                return res.json(payment);
            });
        });
    },

    /**
     * paymentController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        paymentModel.findByIdAndRemove(id, function (err, payment) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the payment.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
