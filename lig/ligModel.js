var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var randomstring = require("randomstring");

var ligSchema = new Schema({
	'name' : String,
	'ref' : String,
	'locks' : [{
	 	type: Schema.Types.ObjectId,
	 	ref: 'lock'
	}],
	'pricing': [],
	'config': {type: Object, default: {}},
	'expiry': Number,
	'is_expired': {type: Boolean, default: false},
	'starts_on' : {type: Date, default: Date.now},
	'ends_on' : {type: Date, default: Date.now}
}, { toJSON: { virtuals: true }});

ligSchema.virtual('lock_logs', {
	ref: 'lock_log',
	localField: '_id',
    foreignField: 'lig'
});

ligSchema.pre('save', function(next) {
	var lig = this;
	if (!lig.isModified('name')) {
	  return next();
	}
	lig.ref = '@'+randomstring.generate(7);
	next();
});

module.exports = mongoose.model('lig', ligSchema);
