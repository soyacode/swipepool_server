var express = require('express');
var router = express.Router();
var ligController = require('./ligController.js');

/*
 * GET
 */
router.get('/', ligController.list);
 
/*
 * GET
 */
router.get('/get-expired', ligController.get_expired);

router.get('/get-lig/:ref', ligController.get_lig);

router.get('/:ref', ligController.show);


/*
 * POST
 */
router.post('/', ligController.create);

router.post('/expire-lig', ligController.expire_lig);

/*
 * PUT
 */
router.put('/:id', ligController.update);

/*
 * DELETE
 */
router.delete('/:id', ligController.remove);

module.exports = router;
