var express = require('express');
var router = express.Router();
var userController = require('./userController.js');
var authMiddleware = require('../middleware');

/*
 * GET
 */
router.get('/', userController.list);

/*
 * GET
 */
router.get('/:id', userController.show);

/* 
 * POST
 */
router.post('/', userController.create);

/**
 * Auth
 */
router.post('/auth/facebook', userController.facebook);

/*
 * PUT 
 */
router.put('/:id', userController.update);

/*
 * DELETE
 */
router.delete('/:id', userController.remove);

router.get('/action/fetch-user',  authMiddleware.ensureAuthenticated, userController.fetch_user);

module.exports = router;
