var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt = require('bcryptjs');

var userSchema = new Schema({
	'email' : String,
	'password' : String,
	'displayName' : String,
	'picture' : String,
	'facebook' : String,
	'balance' : Number,
	'moves' : {type: Number, default: 0},
	'transfer_code' : String,
	'bvn' : Number,
	'account_number' : Number,
	'bank' : String,
	'phone_number' : Number,
	'is_verified' : Boolean
});

userSchema.pre('save', function(next) {
	var user = this;
	if (!user.isModified('password')) {
	  return next();
	}
	bcrypt.genSalt(10, function(err, salt) {
	  bcrypt.hash(user.password, salt, function(err, hash) {
		user.password = hash;
		next();
	  });
	});
  });

module.exports = mongoose.model('user', userSchema);
